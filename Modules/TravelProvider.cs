﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace eventSchedulerService.Modules
{
    public class TravelProvider
    {
        public const string Home = "Home";
        private static TravelProvider _instance;

        private static List<TravelData> _travelData;

        private static List<string> _locations;

        public static TravelProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TravelProvider();
                    FulfillTravelDataList();
                }

                return _instance;
            }
        }

        public TravelData GetTravelData(string from, string to)
        {
            if(from == to) return new TravelData(){Distance = "0km", Duration = new TimeSpan(0, 0, 0, 0), From = from, To = to};
            return _travelData.FirstOrDefault(data => data.From == from && data.To == to);
        }

        public List<string> GetLocations()
        {
            return _locations;
        }

        private static void FulfillTravelDataList()
        {
            _travelData = new List<TravelData>();
            _travelData.Add(new TravelData()
            {
                From = "Home",
                To = "Big Church",
                Duration = new TimeSpan(0, 0, 15, 0),
                Distance = "1.3"
            });
            _travelData.Add(new TravelData()
            {
                From = "Home",
                To = "Tesco",
                Duration = new TimeSpan(0, 0, 8, 0),
                Distance = "0.8"
            });
            _travelData.Add(new TravelData()
            {
                From = "Home",
                To = "University",
                Duration = new TimeSpan(0, 0, 30, 0),
                Distance = "2.5"
            });
            _travelData.Add(new TravelData()
            {
                From = "Home",
                To = "Forum",
                Duration = new TimeSpan(0, 0, 20, 0),
                Distance = "1.8"
            });
            _travelData.Add(new TravelData()
            {
                From = "Home",
                To = "Train Station",
                Duration = new TimeSpan(0, 0, 24, 0),
                Distance = "2.0"
            });
            _travelData.Add(new TravelData(){
                From = "Big Church",
                To = "Home",
                Duration = new TimeSpan(0, 0, 15, 0),
                Distance = "1.3"
            });
            _travelData.Add(new TravelData()
            {
                From = "Tesco",
                To = "Home",
                Duration = new TimeSpan(0, 0, 8, 0),
                Distance = "0.8"
            });
            _travelData.Add(new TravelData()
            {
                From = "University",
                To = "Home",
                Duration = new TimeSpan(0, 0, 30, 0),
                Distance = "2.5"
            });
            _travelData.Add(new TravelData()
            {
                From = "Forum",
                To = "Home",
                Duration = new TimeSpan(0, 0, 20, 0),
                Distance = "1.8"
            });
            _travelData.Add(new TravelData()
            {
                From = "Train Station",
                To = "Home",
                Duration = new TimeSpan(0, 0, 24, 0),
                Distance = "2.0"
            });
            _travelData.Add(new TravelData()
            {
                From = "Big Church",
                To = "Train Station",
                Duration = new TimeSpan(0,0,16,0),
                Distance = "1.4 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Train Station",
                To = "Big Church",
                Duration = new TimeSpan(0, 0, 16, 0),
                Distance = "1.4 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Big Church",
                To = "University",
                Duration = new TimeSpan(0, 0, 36, 0),
                Distance = "2.8 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "University",
                To = "Big Church",
                Duration = new TimeSpan(0, 0, 36, 0),
                Distance = "2.8 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "University",
                To = "Train Station",
                Duration = new TimeSpan(0, 0, 48, 0),
                Distance = "3.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Train Station",
                To = "University",
                Duration = new TimeSpan(0, 0, 48, 0),
                Distance = "3.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Tesco",
                To = "Train Station",
                Duration = new TimeSpan(0, 0, 26, 0),
                Distance = "2.1 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Train Station",
                To = "Tesco",
                Duration = new TimeSpan(0, 0, 26, 0),
                Distance = "2.1 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Big Church",
                To = "Tesco",
                Duration = new TimeSpan(0, 0, 18, 0),
                Distance = "1.4 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Tesco",
                To = "Big Church",
                Duration = new TimeSpan(0, 0, 18, 0),
                Distance = "1.4 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "University",
                To = "Tesco",
                Duration = new TimeSpan(0, 0, 37, 0),
                Distance = "2.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Tesco",
                To = "University",
                Duration = new TimeSpan(0, 0, 37, 0),
                Distance = "2.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Tesco",
                To = "Forum",
                Duration = new TimeSpan(0, 0, 24, 0),
                Distance = "1.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Forum",
                To = "Tesco",
                Duration = new TimeSpan(0, 0, 24, 0),
                Distance = "1.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "University",
                To = "Forum",
                Duration = new TimeSpan(0, 0, 36, 0),
                Distance = "2.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Forum",
                To = "University",
                Duration = new TimeSpan(0, 0, 36, 0),
                Distance = "2.9 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Forum",
                To = "Big Church",
                Duration = new TimeSpan(0, 0, 7, 0),
                Distance = "0.5 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Big Church",
                To = "Forum",
                Duration = new TimeSpan(0, 0, 7, 0),
                Distance = "0.5 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Forum",
                To = "Train Station",
                Duration = new TimeSpan(0, 0, 18, 0),
                Distance = "1.5 km"
            });
            _travelData.Add(new TravelData()
            {
                From = "Train Station",
                To = "Forum",
                Duration = new TimeSpan(0, 0, 18, 0),
                Distance = "1.5 km"
            });

            _locations = new List<string>() { "Home", "Forum", "Train Station", "Big Church", "University", "Tesco" };
        }
    }

    public class TravelData
    {
        public string From { get; set; }
        public string To { get; set; }
        public TimeSpan Duration { get; set; }
        public string Distance { get; set; }
    }
}
