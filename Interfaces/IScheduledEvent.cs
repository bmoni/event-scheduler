﻿using eventSchedulerService.Models;

namespace eventSchedulerService.Interfaces
{
    public interface IScheduledEvent
    {
        string EventID { get; set; }
        DateIntervall EventDate { get; set; }
    }
}
