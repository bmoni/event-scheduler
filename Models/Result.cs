﻿using eventSchedulerService.Enums;

namespace eventSchedulerService.Models
{
    public class Result<T>
    {
        public T Data;
        public ErrorCode? ResultCode;
    }
}
