﻿
namespace eventSchedulerService.Models.Configuration
{
    public class SchedulerConfigurationModel
    {

        public SchedulerConfigurationModel()
        {

        }

        public bool Success { get; set; }

        public string ConnectionUrl { get; set; }
    }
}
