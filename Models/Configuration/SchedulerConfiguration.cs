﻿
namespace eventSchedulerService.Models.Configuration
{
    public class SchedulerConfiguration
    {
        private static SchedulerConfiguration _instance;
        public SchedulerConfigurationModel ConfigurationModel { get; set; }

        public static SchedulerConfigurationModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SchedulerConfiguration(){ConfigurationModel = new SchedulerConfigurationModel()};
                }

                return _instance.ConfigurationModel;
            }
        }
    }
}
