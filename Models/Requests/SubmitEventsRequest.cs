﻿using System;
using eventSchedulerService.Enums;
using Newtonsoft.Json;

namespace eventSchedulerService.Models.Requests
{
    public class SubmitEventsRequest
    {
        [JsonProperty(PropertyName = "algorithm")]
        public string Algorithm;
        
        [JsonProperty(PropertyName = "expectedNumber")]
        public int ExpectedNumber;
        
        [JsonProperty(PropertyName = "heuristic")]
        public string Heuristic;

        public Result<bool> IsValid()
        {
            Result<bool> result;

            if (this.Algorithm == null) result = new Result<bool>() { Data = false, ResultCode = ErrorCode.InvalidAlgorithm };

            else if (this.ExpectedNumber == 0) result = new Result<bool>() { Data = false, ResultCode = ErrorCode.InvalidExpectedNumber };

            else result = new Result<bool>() { Data = true, ResultCode = null };

            return result;
        }
    }
}
