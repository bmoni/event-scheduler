using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace eventSchedulerService.Models.Requests
{
    public class RegistrationRequest
    {
        [Required]
        [JsonProperty(PropertyName = "user-id")]
        public string UserId;

        [Required]
        [JsonProperty(PropertyName = "events")]
        public List<Event> Events;
    }
}