﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace eventSchedulerService.Models
{
    public class EventEntity : Event
    {
        public string UserId { get; set; }

        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string EventID { get; set; }
    }
}
