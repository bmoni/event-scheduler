﻿namespace eventSchedulerService.Models
{
    public static class Algorithms
    {
        public const string ASTAR = "astar";
        public const string BACKTRACK = "backtrack";
        public const string BESTFIRST = "bestfirst";
        public const string BREADTHFIRSTSEARCH = "breadthfirstsearch";
        public const string DEPTHFIRSTSEARCH = "depthfirstsearch";
    }
}
