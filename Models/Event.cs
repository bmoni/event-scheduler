using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace eventSchedulerService.Models
{
    public class Event
    {
        [Required]
        [JsonProperty(PropertyName = "name")]
        public string Name{get;set;}
        [Required]
        [JsonProperty(PropertyName = "targetDateIntervall")]
        public DateIntervall TargetDateIntervall { get; set; }
        [Required]
        [JsonProperty(PropertyName = "duration")]
        public TimeSpan Duration { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        public bool PinnedStatus { get; set; }
        public DateIntervall PinnedDetails { get; set; }
    }
}