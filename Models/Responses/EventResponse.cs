﻿using System;

namespace eventSchedulerService.Models.Responses
{
    public class EventResponse
    {
        public string Name { get; set; }
        public DateIntervall AssignedDate { get; set; }
        public TimeSpan Duration { get; set; }
        public string Location { get; set; }
        public string EventID { get; set; }
    }
}
