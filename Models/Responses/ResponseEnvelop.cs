﻿using System.Collections.Generic;

namespace eventSchedulerService.Models.Responses
{
    public class ResponseEnvelop
    {
        public List<List<EventResponse>> Results { get; set; }

        public Metrics Metrics { get; set; }

        public int ResultCode { get; set; }
    }
}
