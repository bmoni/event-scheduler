﻿namespace eventSchedulerService.Models.Responses
{
    public class Metrics
    {
        public int NodesNumber { get; set; }
        public long RequestTime { get; set; }
        public int OperatorNumber{ get; set; }
    }
}
