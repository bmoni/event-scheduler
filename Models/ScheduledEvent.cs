﻿
using eventSchedulerService.Interfaces;

namespace eventSchedulerService.Models
{
    public class ScheduledEvent: IScheduledEvent
    {
        public string EventID { get; set; }
        public DateIntervall EventDate { get; set; }

        public ScheduledEvent()
        {

        }

        public ScheduledEvent(string eventId, DateIntervall eventDate)
        {
            this.EventID = eventId;
            this.EventDate = new DateIntervall(eventDate.DateFrom, eventDate.DateTo);
        }
    }
}
