using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace eventSchedulerService.Models
{
    public class DateIntervall
    {
        [Required]
        [JsonProperty(PropertyName = "start")]
        public DateTimeOffset DateFrom { get; set; }
        [Required]
        [JsonProperty(PropertyName = "end")]
        public DateTimeOffset DateTo { get; set; }

        public DateIntervall(DateTimeOffset start, DateTimeOffset end)
        {
            this.DateFrom = start;
            this.DateTo = end;
        }

        public double CalculateDistance(DateIntervall target)
        {
            var firstIntervallDay = new DateTimeOffset(this.DateFrom.Year, this.DateFrom.Month, this.DateFrom.Day, 0, 0, 0, new TimeSpan());
            var secondIntervallDay = new DateTimeOffset(target.DateFrom.Year, target.DateFrom.Month, target.DateFrom.Day, 0, 0, 0, new TimeSpan());

            var daysBetweenIntervalls = Math.Abs((firstIntervallDay - secondIntervallDay).Days);

            //todo config when day start and end 
            var a = (this.DateFrom < target.DateFrom ? target.DateFrom - this.DateTo : this.DateTo - target.DateFrom).TotalMinutes
                - (daysBetweenIntervalls * 12 * 60); //todo -?
            return a;
        }

        public bool IsSameDay(DateIntervall target)
        {
            var firstIntervallDay = new DateTimeOffset(this.DateFrom.Year, this.DateFrom.Month, this.DateFrom.Day, 0, 0, 0, new TimeSpan());
            var secondIntervallDay = new DateTimeOffset(target.DateFrom.Year, target.DateFrom.Month, target.DateFrom.Day, 0, 0, 0, new TimeSpan());

            return Math.Abs((firstIntervallDay - secondIntervallDay).Days) == 0;
        }
    }
}