﻿using System.Collections.Generic;
using eventSchedulerService.Models.Requests;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;

namespace eventSchedulerService.Models
{
    public class EventRepository
    {
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfArrays)]
        public List<EventEntity> Events { get; set; }

        public EventRepository(RegistrationRequest request)
        {
            this.Events = new List<EventEntity>();
            
            foreach (var e in request.Events)
            {
                this.Events.Add(new EventEntity()
                {
                    Duration = e.Duration,
                    Name = e.Name,
                    TargetDateIntervall = e.TargetDateIntervall,
                    UserId = request.UserId,
                    Location = e.Location
                });
            }
        }
    }
}
