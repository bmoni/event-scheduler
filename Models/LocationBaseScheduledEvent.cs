﻿using eventSchedulerService.Interfaces;
using eventSchedulerService.Modules;

namespace eventSchedulerService.Models
{
    public class LocationBaseScheduledEvent : IScheduledEvent
    {
        public string EventID { get; set; }
        public DateIntervall EventDate { get; set; }
        public TravelData TravelCost { get; set; }
        public string Location { get; set; }

        public LocationBaseScheduledEvent()
        {
            
        }

        public LocationBaseScheduledEvent(string eventId, DateIntervall eventDate, string location, TravelData travelCost)
        {
            this.EventID = eventId;
            this.EventDate = new DateIntervall(eventDate.DateFrom, eventDate.DateTo);
            this.Location = location;
            this.TravelCost = travelCost;
        }

        public double CalculateDistance(string location)
        {
            if (this.Location == location) return 0.0;

            var travelData = TravelProvider.Instance.GetTravelData(this.Location, location);

            return travelData.Duration.TotalMinutes;
        }
    }
}
