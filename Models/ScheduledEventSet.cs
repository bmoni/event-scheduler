﻿using System;
using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.Interfaces;

namespace eventSchedulerService.Models
{
    public class ScheduledEventSet
    {
        public List<IScheduledEvent> ScheduledEvents { get; set; }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == ((ScheduledEventSet)obj).GetHashCode();
        }

        public override int GetHashCode()
        {
            var orderedList = this.ScheduledEvents.OrderBy(e => e.EventID);
            var code = String.Empty;

            foreach (var o in orderedList)
            {
                code += o.EventID + o.EventDate.DateFrom.ToString() + o.EventDate.DateTo.ToString();
            }

            return code.GetHashCode();
        }
    }
}
