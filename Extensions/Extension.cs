﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace eventSchedulerService.Extensions
{
    public static class Extension
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }
    }
}
