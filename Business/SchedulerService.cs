﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using eventSchedulerService.DataManagement;
using eventSchedulerService.Models;
using eventSchedulerService.Models.Requests;
using eventSchedulerService.Models.Responses;
using eventSchedulerService.Modules;

namespace eventSchedulerService.Business
{
    public static class SchedulerService
    {
        public static List<string> GetLocations()
        {
            return TravelProvider.Instance.GetLocations();
        }

        public static void Register(RegistrationRequest request)
        {
            var eventRepository = new EventRepository(request);

            DataManager.Insert(eventRepository, request.UserId);
        }

        public static ResponseEnvelop Start(string userId, string algorithm, int expectedNumber, string heuristic)
        {
            var stopWatch = Stopwatch.StartNew();
            var response = new ResponseEnvelop();
            GlobalNodes.Initalize();

            try
            {
                var events = DataManager.GetEventEntities(userId);
                response = CreateResponse(SearchEngine.Submit(algorithm, events, expectedNumber, heuristic), events);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error happened in SchedulerService.Start: '{e}'");
            }
            finally
            {
                stopWatch.Stop();
                response.Metrics = new Metrics()
                {
                    RequestTime = stopWatch.ElapsedMilliseconds,
                    NodesNumber = GlobalNodes.Count,
                    OperatorNumber = OperatorCount.InitOperatorCount
                };
            }

            return response;
        }

        public static List<EventEntity> GetEvents(string userId)
        {
            return DataManager.GetEventEntities(userId);
        }

        public static void PinEvents(List<EventResponse> eventResponses, string userId)
        {
            foreach (var response in eventResponses)
            {
                var entity = DataManager.Get(response.EventID, userId);

                entity.PinnedStatus = true;
                entity.PinnedDetails = response.AssignedDate;

                DataManager.Update(entity, userId);
            }
        }

        public static void PinEvent(string eventID, string userId)
        {
            var entity = DataManager.Get(eventID, userId);
            if (entity.PinnedDetails != null)
            {
                entity.PinnedStatus = true;
                DataManager.Update(entity, userId);
            }
        }

        public static void UnpinEvent(string eventID, string userId)
        {
            var entity = DataManager.Get(eventID, userId);
            entity.PinnedStatus = false;
            DataManager.Update(entity, userId);
        }

        private static ResponseEnvelop CreateResponse(HashSet<ScheduledEventSet> scheduledEvents, List<EventEntity> events)
        {
            var orderedSet = OrderEventsByDate(scheduledEvents);
            
            var responseEnvelop = new ResponseEnvelop() { Results = new List<List<EventResponse>>() };

            foreach (var orderedEvents in orderedSet)
            {
                var response = new List<EventResponse>();
                foreach (var e in orderedEvents.ScheduledEvents)
                {
                    var eventR = events.FirstOrDefault(ev => ev.EventID == e.EventID);

                    response.Add(new EventResponse()
                    {
                        AssignedDate = e.EventDate,
                        Duration = eventR.Duration,
                        Name = eventR.Name,
                        Location = eventR.Location,
                        EventID = eventR.EventID
                    });
                }
                responseEnvelop.Results.Add(response);
            }

            return responseEnvelop;
        }

        private static HashSet<ScheduledEventSet> OrderEventsByDate(HashSet<ScheduledEventSet> scheduledEvents)
        {
            var orderedSet = new HashSet<ScheduledEventSet>();

            foreach (var events in scheduledEvents)
            {
                var orderedList = events.ScheduledEvents.OrderBy(e => e.EventDate.DateFrom).ToList();
                orderedSet.Add(new ScheduledEventSet() { ScheduledEvents = orderedList });
            }

            return orderedSet;
        }
    }
}
