﻿using System;
using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.Algorithms;
using eventSchedulerService.AI.Heuristic;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.AI.States;
using eventSchedulerService.Interfaces;
using eventSchedulerService.Models;

namespace eventSchedulerService.Business
{
    public class SearchEngine
    {
        private static TimeSpan _intervallAccuracy = new TimeSpan(1, 0, 0);
        
        public static HashSet<ScheduledEventSet> Submit(string algorithm, List<EventEntity> events, int expectedNumber, string heuristic)
        {
            var operators = CreateOperatorsForEvents(events, heuristic, algorithm);

            switch (algorithm.ToLower())
            {
                case Algorithms.BACKTRACK:
                    {
                        var startState = new Node(operators,
                            new List<IOperator>(),
                            new ScheduledEventSet() { ScheduledEvents = new List<IScheduledEvent>() },
                            PopulateEventAssignementRegistry(events));
                        
                        return new BackTrack().Execute(startState, expectedNumber);
                    }

                case Algorithms.BREADTHFIRSTSEARCH:
                    {
                        var startState = new Node(operators,
                            new List<IOperator>(),
                            new ScheduledEventSet() { ScheduledEvents = new List<IScheduledEvent>() },
                            PopulateEventAssignementRegistry(events));

                        return new BreadthFirstSearch().Execute(startState, expectedNumber);
                    }

                case Algorithms.DEPTHFIRSTSEARCH:
                    {
                        var startState = new Node(operators,
                            new List<IOperator>(),
                            new ScheduledEventSet() { ScheduledEvents = new List<IScheduledEvent>() },
                            PopulateEventAssignementRegistry(events));

                        return new DepthFirstSearch().Execute(startState, expectedNumber);
                    }

                case Algorithms.BESTFIRST:
                    {
                        var heuristicCalculator = GetHeuristicCalculator(heuristic);

                        var startState = new HeuristicNode(operators, 
                            new List<IOperator>(), 
                            new ScheduledEventSet(){ScheduledEvents = new List<IScheduledEvent>()},
                            PopulateEventAssignementRegistry(events), heuristicCalculator);

                        return new BestFirst().Execute(startState, expectedNumber);
                    }

                case Algorithms.ASTAR:
                    {
                        var heuristicCalculator = GetHeuristicCalculator(heuristic);

                        var startState = new CostNode(null, 
                            operators, new List<IOperator>(), 
                            new ScheduledEventSet() { ScheduledEvents = new List<IScheduledEvent>() },
                            PopulateEventAssignementRegistry(events), heuristicCalculator);

                        return new AStar().Execute(startState, expectedNumber);
                    }
            }

            return new HashSet<ScheduledEventSet>();
        }

        private static List<IOperator> CreateOperatorsForEvents(List<EventEntity> events, string heuristic, string algorithm)
        {
            var operators = new List<IOperator>();

            foreach (var e in events)
            {
                if (e.PinnedStatus)
                {
                    operators.AddRange(GenerateOperators(e.PinnedDetails, e.Duration, e.EventID, heuristic, e.Location, algorithm));
                }
                else
                {
                    operators.AddRange(GenerateOperators(e.TargetDateIntervall, e.Duration, e.EventID, heuristic, e.Location, algorithm));
                }
            }

            OperatorCount.InitOperatorCount = operators.Count;

            return operators.OrderBy(op => op.EventDate.DateFrom).ToList();
        }

        private static List<IOperator> GenerateOperators(DateIntervall intervall, TimeSpan duration, string id, string heuristic, string location, string algorithm)
        {
            var operators = new List<IOperator>();

            var localStart = intervall.DateFrom;

            while (localStart.Add(duration) <= intervall.DateTo)
            {
                if (localStart.Hour > 20)
                {
                    localStart = localStart.Add(new TimeSpan(0, 11, 0, 0));
                }
                else if (heuristic?.ToLower() == Heuristic.LOCATIONBASE && 
                    (algorithm.ToLower() == Algorithms.ASTAR || algorithm.ToLower() == Algorithms.BESTFIRST))
                {
                    operators.Add(new LocationBaseOperator(localStart, localStart + duration, id, location));
                }
                else{
                    operators.Add(new Operator(localStart, localStart + duration, id));
                }

                localStart = localStart.Add(_intervallAccuracy);
            }

            return operators;
        }

        private static Dictionary<string, bool> PopulateEventAssignementRegistry(List<EventEntity> events)
        {
            var eventAssignementRegistry = new Dictionary<string, bool>();
            foreach (var e in events)
            {
                eventAssignementRegistry.Add(e.EventID, false);
            }
            return eventAssignementRegistry;
        }

        private static IHeuristicCalculator GetHeuristicCalculator(string heuristic)
        {
            switch (heuristic?.ToLower())
            {
                case Heuristic.LOCATIONBASE:
                {
                    return new LocationBasedHeuristicCalculator();
                }
                case Heuristic.AVERAGE:
                {
                    return new AverageHeuristicCalculator();
                }
                default:
                {
                    return new AverageHeuristicCalculator();
                }
            }
        }
    }
}
