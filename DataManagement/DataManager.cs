﻿using System.Collections.Generic;
using eventSchedulerService.Models;
using eventSchedulerService.Models.Configuration;
using MongoDB.Driver;

namespace eventSchedulerService.DataManagement
{
    public static class DataManager
    {
        private static IMongoDatabase _database;

        public static IMongoDatabase Database
        {
            get {

                if (_database == null)
                {
                    var client = new MongoClient(SchedulerConfiguration.Instance.ConnectionUrl);

                    _database = client.GetDatabase("EventScheduler");
                }

                return _database;
            }
        }

        public static void Insert(EventRepository eventRepository, string userId)
        {
            var collection = Database.GetCollection<EventEntity>("EventRepository");

            collection.InsertMany(eventRepository.Events);
        }

        public static List<EventEntity> GetEventEntities(string userId)
        {
            var collection = Database.GetCollection<EventEntity>("EventRepository");
            var a = collection.Find(e=> e.PinnedStatus || !e.PinnedStatus).ToList();
            return collection.Find(e => e.UserId == userId).ToList();
        }
        
        public static void Update(EventEntity entity, string userId)
        {
            Database.GetCollection<EventEntity>("EventRepository").ReplaceOne(e => e.UserId == userId && e.EventID == entity.EventID, entity);
        }

        public static EventEntity Get(string eventID, string userId)
        {
            return Database.GetCollection<EventEntity>("EventRepository").Find(e => e.UserId == userId && e.EventID == eventID).FirstOrDefault();
        }
    }
}
