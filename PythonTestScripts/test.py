import glob
import json
import re
import calendar;
import time;
import requests
import csv

baseUrl = 'http://localhost:50997'
createEventUrl = baseUrl + '/api/Contact/register'
submitEventUrl = baseUrl + '/api/Contact/submit/'
def getScenarioName(f):
   return re.search('.\/scenarios\\\(.*).json',f).group(1)

def getEmail(f):
   email = 'AnalyticsScenario-'
   email += getScenarioName(f)
   email += '@tester.com'
   return email
   
def submitEvaluation(email,alg,mode,heur):
   data = {'algorithm': alg,  'expectedNumber': mode,  'heuristic': heur}
   headers = {'Content-type': 'text/json'}
   response = requests.post(submitEventUrl+email, data=json.dumps(data),headers=headers)
   json_data = json.loads(response.text)
   return json_data['metrics']
   
files = [f for f in glob.glob('./scenarios/*.json',recursive=False)]
algs = ['breadthfirstsearch','depthfirstsearch','backtrack','bestfirst','astar']
modes = [1,5]
heuristic = ['average','locationbase']

print('===========================')
print('Generating test results....')
print('===========================')
print('')
with open('Results.csv', mode='w') as analytics_file:
   analytics_writer = csv.writer(analytics_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
   for f in files:
      with open(f) as json_file:
         email = getEmail(f)
         scenarioName = getScenarioName(f)
         print('>Scenario:'+scenarioName)
         print('>Email:'+email)
         print('----------------------')
         print('')
         for mode in modes:  
            for heur in heuristic:  
               for alg in algs:         
                  print('>>Algorithm:' + alg)
                  print('>>Heuristic:' + heur)
                  print('>>Expected Number:' + str(mode))
                  if alg == 'astar' or alg == 'bestfirst' :
                     response = submitEvaluation(email,alg,mode,heur)
                     print('>>>Done')
                     print('')
                     analytics_writer.writerow([scenarioName,alg, str(mode),heur, str(response['requestTime']), str(response['nodesNumber']), str(response['operatorNumber'])])
                  else :
                     if heur == 'average' :
                        response = submitEvaluation(email,alg,mode,'n/a')
                        print('>>>Override heuristic from average to n/a')
                        print('>>>Done')
                        print('')
                        analytics_writer.writerow([scenarioName,alg, str(mode),'n/a', str(response['requestTime']), str(response['nodesNumber']), str(response['operatorNumber'])])
                     else :
                        print('>>>Skipped heuristic')
                        print('>>>Done')
                        print('')
