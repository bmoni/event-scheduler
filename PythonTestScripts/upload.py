import glob
import json
import re
import calendar
import time
import requests
import csv

baseUrl = 'http://localhost:50997'
createEventUrl = baseUrl + '/api/Contact/register'
def getScenarioName(f):
   return re.search('.\/scenarios\\\(.*).json',f).group(1)

def getEmail(f):
   email = 'AnalyticsScenario-'
   email += getScenarioName(f)
   email += '@tester.com'
   return email

def createEvent(data):
   headers = {'Content-type': 'text/json'}
   response = requests.post(createEventUrl, data=json.dumps(data),headers=headers)
   return response
   
files = [f for f in glob.glob('./scenarios/*.json',recursive=False)]

print('====================')
print('Upload test data....')
print('====================')
print('')
for f in files:
   print('>'+f)
   with open(f) as json_file:
      data = json.load(json_file)
      email = getEmail(f)
      print('>>>Assign email address:'+email)
      data['user-id'] = email
      #testData(data)
      createEvent(data)
      print('>>>Done')
      print('')
         
      
