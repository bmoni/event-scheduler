﻿using System.Collections.Generic;
using eventSchedulerService.AI.Heuristic;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.States
{
    public class HeuristicNode : Node, INode
    {
        public IHeuristicCalculator HeuristicCalculator;

        public double Heuristic { get; set; }
        
        public HeuristicNode(List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results,
            Dictionary<string, bool> eventAssignementRegistry, IHeuristicCalculator heuristicCalculator) : 
            base(operators, processedOperators, results, eventAssignementRegistry)
        {
            this.HeuristicCalculator = heuristicCalculator;
            this.Heuristic = heuristicCalculator.CalculateHeuristic(operators, results);
        }

        public INode BuildChild(INode parent, List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results, Dictionary<string, bool> eventAssignementRegistry)
        {
            return new HeuristicNode(operators, processedOperators, results, eventAssignementRegistry, this.HeuristicCalculator);
        }
    }
}
