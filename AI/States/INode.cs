﻿using System.Collections.Generic;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.States
{
    public interface INode
    {
        List<IOperator> Operators { get; set; }

        List<IOperator> ProcessedOperators { get; set; }

        Dictionary<string, bool> EventAssignementRegistry { get; set; }

        INode BuildChild(INode parent, List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results, Dictionary<string, bool> eventAssignementRegistry);
            
    }
}
