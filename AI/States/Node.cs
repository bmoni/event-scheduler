﻿using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.States
{
    public class Node : State, INode
    {
        public List<IOperator> Operators { get; set; }
        public List<IOperator> ProcessedOperators { get; set; }
        public Dictionary<string, bool> EventAssignementRegistry { get; set; }

        public Node()
        {
            
        }
        public Node(List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results,
            Dictionary<string, bool> eventAssignementRegistry)
        {
            this.Operators = operators;
            this.ProcessedOperators = processedOperators;
            this.Results = results;
            this.EventAssignementRegistry = eventAssignementRegistry;
        }
        
        public override bool IsFinal()
        {
            if (this.Operators?.Count == 0
                && this.Results.ScheduledEvents.Select(r => r.EventID).Count() == this.EventAssignementRegistry.Keys.Count)
            {
                return true;
            }

            return false;
        }

        public override bool IsValid()
        {
            var unassignedEvents = this.EventAssignementRegistry.Where(a => a.Value == false).Select(a => a.Key);

            foreach (var e in unassignedEvents)
            {
                if (this.Operators.All(op => op.EventID != e)) return false;
            }

            return true;
        }

        public INode BuildChild(INode parent, List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results, Dictionary<string, bool> eventAssignementRegistry)
        {
            return new Node(operators, processedOperators, results, eventAssignementRegistry);
        }
    }
}
