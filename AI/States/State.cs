﻿using eventSchedulerService.Models;

namespace eventSchedulerService.AI.States
{
    public abstract class State
    {
        public ScheduledEventSet Results { get; set; }

        public abstract bool IsFinal();

        public abstract bool IsValid();
    }
}
