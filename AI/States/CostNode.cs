﻿using System.Collections.Generic;
using eventSchedulerService.AI.Heuristic;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.States
{
    public class CostNode : HeuristicNode, INode
    {
        public double Cost { get; set; }

        public CostNode(CostNode parent, List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results,
            Dictionary<string, bool> eventAssignementRegistry, IHeuristicCalculator heuristicCalculator) : 
            base (operators, processedOperators, results, eventAssignementRegistry, heuristicCalculator)
        {
            this.Cost = CalculateCost(parent);
        }

        public double CalculateCost(CostNode parent)
        {
            return parent == null ? 0 : parent.Heuristic + parent.Cost;
        }

        public INode BuildChild(INode parent, List<IOperator> operators, List<IOperator> processedOperators, ScheduledEventSet results, Dictionary<string, bool> eventAssignementRegistry)
        {
            return new CostNode((CostNode)parent, operators, processedOperators, results, eventAssignementRegistry, this.HeuristicCalculator);
        }
    }
}
