﻿using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.States;
using eventSchedulerService.Extensions;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Algorithms
{
    public class BestFirst : AbstractAlgorithm
    {
        private Dictionary<HeuristicNode, int> _openSet = new Dictionary<HeuristicNode, int>();

        public HashSet<ScheduledEventSet> Execute(HeuristicNode startState, int expectedNumber)
        {
            ExpectedNumberOfFinalEvents = expectedNumber;
            _openSet.Add(startState, startState.Results.GetHashCode());

            Submit();

            return FinalEvents;
        }

        protected void Submit()
        {
            while (FinalEvents.Count < ExpectedNumberOfFinalEvents && !_openSet.IsNullOrEmpty())
            {
                var node = _openSet.Keys.OrderBy(s => s.Heuristic).First();

                if (node.IsFinal())
                {
                    AddEventsToFinal(node.Results);
                }
                else if (node.IsValid())
                {
                    foreach (var op in node.Operators)
                    {
                        var childNode = (HeuristicNode)op.Execute(node);
                        var childNodeHash = childNode.Results.GetHashCode();

                        if (_openSet.ContainsValue(childNodeHash)) continue;

                        GlobalNodes.Count++;
                        _openSet.Add(childNode, childNodeHash);
                    }
                }

                _openSet.Remove(node);
            }
        }
    }
}
