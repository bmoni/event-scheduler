﻿using System.Collections.Generic;
using eventSchedulerService.AI.States;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Algorithms
{
    //melysegi
    public class DepthFirstSearch : AbstractAlgorithm
    {
        public HashSet<ScheduledEventSet> Execute(Node startState, int expectedNumber)
        {
            ExpectedNumberOfFinalEvents = expectedNumber;
            Submit(startState);
            return FinalEvents;
        }

        protected bool Submit(Node node)
        {
            if (node.IsFinal())
            {
                return AddEventsToFinal(node.Results);
            }
            if (!node.IsValid()) return false;

            var childNodes = new List<Node>();
            foreach (var op in node.Operators)
            {
                GlobalNodes.Count++;
                childNodes.Add((Node)op.Execute(node));
            }

            foreach (var childNode in childNodes)
            {
                if (Submit(childNode)) return true;
            }

            return false;
        }
    }
}
