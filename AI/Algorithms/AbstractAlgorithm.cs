﻿using System.Collections.Generic;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Algorithms
{
    public abstract class AbstractAlgorithm
    {
        protected HashSet<ScheduledEventSet> FinalEvents = new HashSet<ScheduledEventSet>();

        protected int ExpectedNumberOfFinalEvents { get; set; }

        protected bool AddEventsToFinal(ScheduledEventSet finalEventSet)
        {
            FinalEvents.Add(finalEventSet);

            if(FinalEvents.Count == ExpectedNumberOfFinalEvents)
            {
                return true;
            }

            return false;
        }
    }
}
