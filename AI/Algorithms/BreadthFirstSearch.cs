using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.States;
using eventSchedulerService.Extensions;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Algorithms
{
    //szelessegi
    public class BreadthFirstSearch : AbstractAlgorithm
    {
        public HashSet<ScheduledEventSet> Execute(Node startState, int expectedNumber)
        {
            ExpectedNumberOfFinalEvents = expectedNumber;
            Submit(new BreadhtFirstLayerWrapper() { ChildNodes = new List<Node>() { startState } });
            return FinalEvents;
        }

        protected void Submit(BreadhtFirstLayerWrapper wrapper)
        {
            ProcessWrapper(wrapper);
        }

        public void ProcessWrapper(BreadhtFirstLayerWrapper wrapper)
        {
            if (wrapper.ContainsFinalStates(out var finalStates))
            {
                foreach(var finalState in finalStates)
                {
                    if (AddEventsToFinal(finalState.Results))
                    {
                        return;
                    }
                }
                return; //???
            }

            ProcessWrapper(wrapper.Extract());
        }
    }

    public class BreadhtFirstLayerWrapper
    {
        public List<Node> ChildNodes;

        public BreadhtFirstLayerWrapper Extract()
        {
            var wrapper = new BreadhtFirstLayerWrapper() { ChildNodes = new List<Node>() };

            foreach (var node in ChildNodes)
            {
                if (!node.IsValid()) continue;

                foreach (var op in node.Operators)
                {
                    GlobalNodes.Count++;
                    wrapper.ChildNodes.Add((Node)op.Execute(node));
                }
            }

            return wrapper;
        }

        public bool ContainsFinalStates(out List<Node> finalStates)
        {
            //level number of the tree equals to the number of events => childStates contain all result
            finalStates = ChildNodes.Where(state => state.IsFinal()).ToList();

            return !finalStates.IsNullOrEmpty();
        }
    }
}