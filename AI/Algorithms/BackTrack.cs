﻿using eventSchedulerService.AI.States;
using eventSchedulerService.Models;
using System.Collections.Generic;

namespace eventSchedulerService.AI.Algorithms
{
    public class BackTrack : AbstractAlgorithm
    {
        public HashSet<ScheduledEventSet> Execute(Node startState, int expectedNumber)
        {
            ExpectedNumberOfFinalEvents = expectedNumber;
            Submit(startState);
            return FinalEvents;
        }

        protected bool Submit(Node node)
        {
            if (node.IsFinal())
            {
                return AddEventsToFinal(node.Results);
            }

            if (node.IsValid())
            {
                foreach (var op in node.Operators)
                {
                    if (node.ProcessedOperators.Contains(op)) continue;
                    var nextState = op.Execute(node);
                    GlobalNodes.Count++;
                    if (Submit((Node)nextState)) return true;
                }
            }
            return false;
        }
    }
}
