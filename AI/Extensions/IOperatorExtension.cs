﻿using System.Collections.Generic;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.AI.States;

namespace eventSchedulerService.AI.Extensions
{
    public static class IOperatorExtension
    {
        public static List<IOperator> PrepareProcessedOperators(this IOperator _operator, INode parent)
        {
            var processed = new List<IOperator>();

            foreach (var op in parent.ProcessedOperators)
            {
                processed.Add(new Operator(op.EventDate.DateFrom, op.EventDate.DateTo, op.EventID));
            }

            processed.Add(new Operator(_operator.EventDate.DateFrom, _operator.EventDate.DateTo, _operator.EventID));

            return processed;
        }

        public static Dictionary<string, bool> PrepareAssignedDates(this IOperator _operator, INode parent)
        {
            var assignedDates = new Dictionary<string, bool>();

            foreach (var assignedDate in parent.EventAssignementRegistry)
            {
                assignedDates.Add(assignedDate.Key, assignedDate.Value);
            }

            assignedDates[_operator.EventID] = true;

            return assignedDates;
        }
    }
}
