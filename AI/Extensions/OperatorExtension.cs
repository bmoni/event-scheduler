﻿using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.AI.States;
using eventSchedulerService.Interfaces;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Extensions
{
    public static class OperatorExtension
    {
        public static List<IOperator> PrepareOperators(this Operator _operator, INode parent)
        {
            //1. remove used operators from parent.operators using event id 
            var thisEventOps = parent.Operators.Where(op => op.EventID == _operator.EventID).ToList();

            //2. remove operators with overlap datetimes
            var usedDateOps = parent.Operators.Where(op => _operator.EventDate.DateFrom < op.EventDate.DateTo
                    && op.EventDate.DateFrom < _operator.EventDate.DateTo).ToList();

            var copiedList = new List<IOperator>();
            var operators = parent.Operators.Except(thisEventOps).Except(usedDateOps).ToList();
            foreach (var op in operators)
            {
                copiedList.Add(new Operator(op.EventDate.DateFrom, op.EventDate.DateTo, op.EventID));
            }

            return copiedList.OrderBy(op => op.EventDate.DateFrom).ToList();
        }

        public static ScheduledEventSet PrepareResults(this Operator _operator, INode parent)
        {
            ScheduledEventSet results = new ScheduledEventSet()
            {
                ScheduledEvents = new List<IScheduledEvent>()
            };

            foreach (var r in ((Node)parent).Results.ScheduledEvents)
            {
                results.ScheduledEvents.Add(new ScheduledEvent(r.EventID, r.EventDate));
            }

            results.ScheduledEvents.Add(new ScheduledEvent(_operator.EventID, _operator.EventDate));

            return new ScheduledEventSet() { ScheduledEvents = results.ScheduledEvents.OrderBy(e => e.EventDate.DateFrom).ToList() };
        }
    }
}
