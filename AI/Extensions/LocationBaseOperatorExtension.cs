﻿using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.AI.States;
using eventSchedulerService.Interfaces;
using eventSchedulerService.Models;
using eventSchedulerService.Modules;

namespace eventSchedulerService.AI.Extensions
{
    public static class LocationBaseOperatorExtension
    {
        public static List<IOperator> PrepareOperators(this LocationBaseOperator _operator, INode parent)
        {
            //1. check scheduledevents that overlap result event dates extended with traveling time
            var travelTimeConflicts = ((Node)parent).Results.ScheduledEvents.Where(e =>
                {
                    var travelingTime = TravelProvider.Instance.GetTravelData(((LocationBaseScheduledEvent)e).Location, _operator.Location).Duration;
                    if (_operator.EventDate.DateFrom - travelingTime < e.EventDate.DateTo
                        && e.EventDate.DateFrom < _operator.EventDate.DateTo + travelingTime) return true;
                    return false;
                }
            );

            if (travelTimeConflicts.Count() != 0) return new List<IOperator>();

            //2. remove used operators from parent.operators using event id 
            var thisEventOps = parent.Operators.Where(op => op.EventID == _operator.EventID).ToList();

            //3. remove operators with overlap datetimes
            var usedDateOps = parent.Operators.Where(op => _operator.EventDate.DateFrom < op.EventDate.DateTo
                                                           && op.EventDate.DateFrom < _operator.EventDate.DateTo).ToList();

            var copiedList = new List<IOperator>();
            var operators = parent.Operators.Except(thisEventOps).Except(usedDateOps).ToList();
            foreach (var op in operators)
            {
                copiedList.Add(new LocationBaseOperator(op.EventDate.DateFrom, op.EventDate.DateTo, op.EventID, ((LocationBaseOperator)op).Location));
            }

            return copiedList.OrderBy(op => op.EventDate.DateFrom).ToList();
        }

        public static ScheduledEventSet PrepareResults(this LocationBaseOperator _operator, INode parent)
        {
            ScheduledEventSet results = new ScheduledEventSet()
            {
                ScheduledEvents = new List<IScheduledEvent>()
            };

            foreach (var r in ((Node)parent).Results.ScheduledEvents)
            {
                var locationBaseEvent = (LocationBaseScheduledEvent)r;
                results.ScheduledEvents.Add(new LocationBaseScheduledEvent(r.EventID, r.EventDate, locationBaseEvent.Location, locationBaseEvent.TravelCost));
            }

            results.ScheduledEvents.Add(new LocationBaseScheduledEvent(_operator.EventID, _operator.EventDate, _operator.Location, new TravelData()));

            OverrideResultsTravelCost(_operator, results.ScheduledEvents);

            return new ScheduledEventSet() { ScheduledEvents = results.ScheduledEvents.OrderBy(e => e.EventDate.DateFrom).ToList() };
        }

        private static void OverrideResultsTravelCost(LocationBaseOperator _operator, List<IScheduledEvent> scheduledEvents)
        {
            var orderedScheduledEvents = scheduledEvents.OrderBy(e => e.EventDate.DateFrom).ToList();

            var thisEventIndex = orderedScheduledEvents.FindIndex(e => e.EventID == _operator.EventID);

            var thisEvent = (LocationBaseScheduledEvent)orderedScheduledEvents[thisEventIndex];
            if (thisEventIndex == 0)
            {
                thisEvent.TravelCost = TravelProvider.Instance.GetTravelData(TravelProvider.Home, thisEvent.Location);
            }
            else
            {
                var previousEvent = (LocationBaseScheduledEvent)orderedScheduledEvents[thisEventIndex - 1];

                if (previousEvent.EventDate.IsSameDay(thisEvent.EventDate))
                {
                    thisEvent.TravelCost = TravelProvider.Instance.GetTravelData(previousEvent.Location, thisEvent.Location);
                }
                else
                {
                    thisEvent.TravelCost = TravelProvider.Instance.GetTravelData(TravelProvider.Home, thisEvent.Location);
                }
            }

            if (thisEventIndex != orderedScheduledEvents.Count - 1)
            {
                var nextEvent = (LocationBaseScheduledEvent)orderedScheduledEvents[thisEventIndex + 1];

                if (thisEvent.EventDate.IsSameDay(nextEvent.EventDate))
                {
                    nextEvent.TravelCost = TravelProvider.Instance.GetTravelData(thisEvent.Location, nextEvent.Location);
                }
            }
        }
    }
}
