﻿using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Extensions;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Heuristic
{
    public class AverageHeuristicCalculator : IHeuristicCalculator
    {
        public double CalculateHeuristic(List<IOperator> operators, ScheduledEventSet results)
        {
            if (results.ScheduledEvents.IsNullOrEmpty())
            {
                return 0;
            }

            if (operators.IsNullOrEmpty())
            {
                var orderedResult = results.ScheduledEvents.OrderBy(e => e.EventDate.DateFrom).ToList();
                var sumOfDistanceBtwEvents = 0.0;
                for (int i = 0; i < orderedResult.Count - 1; i++)
                {
                    sumOfDistanceBtwEvents += orderedResult[i].EventDate.CalculateDistance(orderedResult[i + 1].EventDate);
                }

                return sumOfDistanceBtwEvents;
            }
            else
            {
                var heuristic = 0.0;

                var orderedOperators = operators.OrderBy(op => op.EventID).ToList();

                // max events count 
                var eventCounter = 1;
                // event/op
                var opEventCounter = 0;
                var opEventSum = 0.0;
                var currentEventId = orderedOperators.First().EventID;
                foreach (var op in orderedOperators)
                {
                    if (currentEventId != op.EventID)
                    {
                        heuristic += opEventSum / opEventCounter;
                        opEventSum = 0;
                        opEventCounter = 0;

                        eventCounter++;
                        currentEventId = op.EventID;
                    }

                    opEventCounter++;

                    var orderedResults = results.ScheduledEvents.OrderBy(e => e.EventDate.CalculateDistance(op.EventDate)).ToList();

                    var firstResult = orderedResults.First();
                    opEventSum += firstResult.EventDate.CalculateDistance(op.EventDate);
                }

                return heuristic / eventCounter;
            }
        }
    }
}
