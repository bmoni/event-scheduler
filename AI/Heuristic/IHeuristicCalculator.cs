﻿using System.Collections.Generic;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Heuristic
{
    public interface IHeuristicCalculator
    {
        double CalculateHeuristic(List<IOperator> operators, ScheduledEventSet results);
    }
}
