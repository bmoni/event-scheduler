﻿using System;
using System.Collections.Generic;
using System.Linq;
using eventSchedulerService.AI.Operators;
using eventSchedulerService.Extensions;
using eventSchedulerService.Models;
using eventSchedulerService.Modules;

namespace eventSchedulerService.AI.Heuristic
{
    public class LocationBasedHeuristicCalculator : IHeuristicCalculator
    {
        public double CalculateHeuristic(List<IOperator> operators, ScheduledEventSet results)
        {
            if (results.ScheduledEvents.IsNullOrEmpty())
            {
                return 0;
            }

            if (operators.IsNullOrEmpty())
            {
                var totalTravelingTime = results.ScheduledEvents.Sum(e => ((LocationBaseScheduledEvent)e).TravelCost.Duration.TotalMinutes);
                return totalTravelingTime / results.ScheduledEvents.Count;
            }
            else
            {
                var totalTravelingTime = 0.0;
                foreach (var op in operators)
                {
                    var orderedResults = results.ScheduledEvents.OrderBy(e => e.EventDate.CalculateDistance(op.EventDate)).ToList();

                    var firstResult = orderedResults.First();
                    totalTravelingTime += TravelProvider.Instance.GetTravelData(((LocationBaseScheduledEvent)firstResult).Location,
                        ((LocationBaseOperator)op).Location).Duration.TotalMinutes;
                }

                return totalTravelingTime / operators.Count;
            }
        }
    }
}
