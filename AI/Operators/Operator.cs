﻿using eventSchedulerService.AI.States;
using eventSchedulerService.Models;
using System;
using eventSchedulerService.AI.Extensions;

namespace eventSchedulerService.AI.Operators
{
    public class Operator : IOperator
    {
        public string EventID { get; set; }

        public DateIntervall EventDate { get; set; }

        public Operator()
        {

        }

        public Operator(DateTimeOffset start, DateTimeOffset end, string id)
        {
            this.EventDate = new DateIntervall(start, end);
            this.EventID = id;
        }

        public INode Execute(INode parent)
        {
            return parent.BuildChild(parent, 
                this.PrepareOperators(parent), 
                this.PrepareProcessedOperators(parent), 
                this.PrepareResults(parent), 
                this.PrepareAssignedDates(parent));
        }
    }
}
