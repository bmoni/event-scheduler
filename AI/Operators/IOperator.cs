﻿using eventSchedulerService.AI.States;
using eventSchedulerService.Models;

namespace eventSchedulerService.AI.Operators
{
    public interface IOperator
    {
        string EventID { get; set; }

        DateIntervall EventDate { get; set; }

        INode Execute(INode parent);
    }
}
