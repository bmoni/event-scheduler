﻿using eventSchedulerService.AI.States;
using eventSchedulerService.Models;
using System;
using eventSchedulerService.AI.Extensions;

namespace eventSchedulerService.AI.Operators
{
    public class LocationBaseOperator : IOperator
    {
        public string EventID { get; set; }

        public DateIntervall EventDate { get; set; }

        public string Location { get; set; }

        public LocationBaseOperator()
        {

        }

        public LocationBaseOperator(DateTimeOffset start, DateTimeOffset end, string id, string location)
        {
            this.EventDate = new DateIntervall(start, end);
            this.EventID = id;
            this.Location = location;
        }

        public INode Execute(INode parent)
        {
            return parent.BuildChild(parent, 
                this.PrepareOperators(parent), 
                this.PrepareProcessedOperators(parent), 
                this.PrepareResults(parent), 
                this.PrepareAssignedDates(parent));
        }
    }
}
