﻿namespace eventSchedulerService.Enums
{
    public enum ErrorCode
    {
        RequestBodyNull = 1100,
        InvalidAlgorithm = 1101,
        InvalidExpectedNumber = 1102,
        InvalidCalendarIntervall = 1103,
        InvalidHeuristic = 1104
    }
}
