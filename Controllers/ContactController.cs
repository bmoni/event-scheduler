using eventSchedulerService.Business;
using eventSchedulerService.Models;
using eventSchedulerService.Models.Requests;
using eventSchedulerService.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace eventSchedulerService.Controllers
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        [HttpGet("ping")]
        public string Ping()
        {
            return "pong";
        }

        [HttpGet("locations")]
        public List<string> GetLocations()
        {
            return SchedulerService.GetLocations();
        }

        //todo fix void
        [HttpPost("register")]
        public void RegisterEvents([FromBody] RegistrationRequest request)
        {
            if (request == null)
            {
                Response.StatusCode = 400;
                return;
            }

            SchedulerService.Register(request);
        }

        [HttpPost("submit/{userId}")]
        public ResponseEnvelop Submit([FromBody] SubmitEventsRequest request, string userId)
        {
            if (request == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseEnvelop();
            }

            var requestValidationResult = request.IsValid();
            if (!requestValidationResult.Data)
            {
                Response.StatusCode = (int)HttpStatusCode.NotAcceptable;
                return new ResponseEnvelop() { ResultCode = (int)requestValidationResult.ResultCode };
            }

            return SchedulerService.Start(userId,
                request.Algorithm,
                request.ExpectedNumber,
                request.Heuristic);
        }

        [HttpGet("getevents/{userId}")]
        public List<EventEntity> GetEvents(string userId)
        {
            var a = SchedulerService.GetEvents(userId);
            return a;
        }
        //todo fix void
        [HttpPut("applyEvents/{userId}")]
        public void ApplyEvents([FromBody] List<EventResponse> eventResponses, string userId)
        {
            SchedulerService.PinEvents(eventResponses, userId);
        }
        //todo fix void
        [HttpPut("event/pin/{eventId}/{userId}")]
        public void RePinEvent(string eventId, string userId)
        {
            SchedulerService.PinEvent(eventId, userId);
        }
        //todo fix void
        [HttpPut("event/unpin/{eventId}/{userId}")]
        public void UnPinEvent(string eventId, string userId)
        {
            SchedulerService.UnpinEvent(eventId, userId);
        }
    }
}